#include "Donor.h"
#include "Transaction.h"

Donor::Donor(std::string first, std::string nlast)
{
	name = first;
	last = nlast;
	Transactions = new std::map<int, Transaction*>();
}

Donor::~Donor()
{

}

void Donor::addTransaction(int id, float amount, int dept)
{
	std::map<int, Transaction*> &map = *Transactions;
	Transaction* temp = new Transaction(amount, dept);
	map.insert({ id, temp });
}

void Donor::deleteTransaction(int tID)
{
	std::map<int, Transaction*> &map = *Transactions;
	map.erase(tID);
}

std::string Donor::getName()
{
	return (name + " " + last);
}

std::map<int, Transaction*> &Donor::getMap()
{
	return *Transactions;
}
