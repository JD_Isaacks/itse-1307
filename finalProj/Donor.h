#pragma once

#include <map>
#include <string>

class Transaction;

class Donor
{
public:
	Donor(std::string name, std::string nlast);
	~Donor();
	void addTransaction(int id, float amount, int dept);
	void deleteTransaction(int tID);
	std::string getName();
	std::map<int, Transaction*> &getMap();
private:
	std::string name, last;
	std::map <int, Transaction*>* Transactions;
};
