#include "Transaction.h"

Transaction::Transaction()
{

}

Transaction::Transaction(float a, int b)
{
	amount = a;
	dept = Depts (b);
}

Transaction::~Transaction()
{

}

void Transaction::change_amount(float a)
{
	amount = a;
}

void Transaction::change_dept(int b)
{
	dept = Depts(b);
}

float Transaction::get_amount()
{
	return amount;
}

int Transaction::get_dept()
{
	return dept;
}
