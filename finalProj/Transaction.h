#pragma once
#include <string>

class Transaction
{
public:
	Transaction();
	Transaction(float a, int b);
	~Transaction();
	void change_amount(float a);
	void change_dept(int b);
	float get_amount();
	int get_dept();
private:
	float amount;
	enum Depts { CS, Eng, Math, Nurs } dept;
};
