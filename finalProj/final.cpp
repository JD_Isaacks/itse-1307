/****************************************
		Final Project ITSE 1307
			James Isaacks
*****************************************
*
*	Purpose:
*	
*	- Handle and track donations for 4 departments of a fictional school. 
*	- Allow the user to record multiple transactions per donor.
*	- Allow the user to find and edit tranactions based on transaction or donor.
*	- Subtotal and Reconcile all activity.
*
*	Last update: 
******************************************/
#include <cmath>
#include <iostream>
#include <iomanip>
#include <numeric>
#include <stdio.h>
#include <vector>
#include <utility>
#include "donor.h"
#include "transaction.h"

void menuMain();
void menuReco();
void menuUtil();
void addDonation(int dID);
void addDonors();
void displayDonations(int dID);
void displayDonor(int dID);
void displayDonorList();
void editDonation(int eID, int tID);
void deleteDonation(int eID, int tID);

int dID = 0;
int tID = 0;
std::map <int, Donor*> Donors;
const char * Dept[] = {"Computer Science", "English", "Math", "Nursing"};

int main()
{
	int leave;
	std::cout << std::setprecision(2) << std::fixed;

	do
	{
		menuMain();
		leave = std::getchar();
	} while (leave != 27);
	return 0;
}

void menuMain()
{
	int c;
	do 
	{
		system("cls");
		std::cout << "Donation Tracker v0.1\n\nPlease Select an Option\n1: New Donation\n2: Search, Edit, or Delete\n3: Reconcile" << std::endl;
		std::cin >> c;
		std::cin.ignore();
		system("cls");
		switch (c)
		{
		case 1:
			addDonors();
			break;
		case 2:
			menuUtil();
			break;
		case 3:
			menuReco();
			break;
		default:
			break;
		}
	} while (c != 27);
}

void menuReco()													// Total everything at the end.
{
	float total = 0;
	float total_1 = 0;
	float total_2 = 0;
	float total_3 = 0;
	float total_4 = 0;

	for (std::map<int, Donor*>::iterator it = Donors.begin(); it != Donors.end(); ++it)
	{
		Donor tempDonor = *it->second;
		std::map<int, Transaction*> &tempMap = tempDonor.getMap();
		std::cout << tempDonor.getName() << std::endl;
		for (std::map<int, Transaction*>::iterator it = tempMap.begin(); it != tempMap.end(); ++it)
		{
			Transaction tempTran = *it->second;
			int dept = tempTran.get_dept();
			std::cout << "\t" << Dept[dept] << ": $" << tempTran.get_amount() << std::endl;
			if(dept == 1)
			{
				total_1 += tempTran.get_amount();
			}
			else if(dept == 2)
			{
				total_2 += tempTran.get_amount();
			}
			else if(dept == 3)
			{
				total_3 += tempTran.get_amount();
			}
			else if(dept == 4)
			{
				total_4 += tempTran.get_amount();
			}
		}
	}
	total = total_1 + total_2 + total_3 + total_4;
	std::cout << "Computer Science: $" << total_1 << "\nEnglish: $" << total_2 << "\nMath: $" << total_3 << "\nNursing: $" << total_4 << "\nTOTAL DONATIONS: $" << total << std::endl;
	system("pause");
	system("cls");
}

void menuUtil()
{
	int c;
	int ID;
	do
	{
		system("cls");
		displayDonorList();
		std::cout << "Please select donor or enter donor ID:" << std::endl;
		while (!(std::cin >> ID))
		{
			std::cin.clear();
			std::cin.ignore(99999999, '\n');
			std::cout << "Invalid entry. Please select donor or enter donor ID:" << std::endl;
		}
		displayDonor(ID);
		std::cout << "What would you like to do?\n1: Add a donation\n2: Edit a donation\n3: Delete a donation\n 4: Return to Menu" << std::endl;
		std::cin >> c;
		std::cin.ignore();
		switch (c)
		{
		case 1:
			addDonation(ID);
			break;
		case 2:
			std::cout << "Which donation would you like to edit?" << std::endl;
			displayDonations(ID);
			while (!(std::cin >> c))
			{
				std::cin.clear();
				std::cin.ignore(99999999, '\n');
				std::cout << "Invalid entry. Please select a donation by its ID:" << std::endl;
			}
			editDonation(ID, c);
			break;
		case 3:
			std::cout << "Which donation would you like to delete?" << std::endl;
			displayDonations(ID);
			while (!(std::cin >> c))
			{
				std::cin.clear();
				std::cin.ignore(99999999, '\n');
				std::cout << "Invalid entry.  Please select a donation by its ID:" << std::endl;
			}
			deleteDonation(ID, c);
			break;
		case 4:
			menuMain();
		default:
			menuMain();
		}
	}while (c != 27);
}

void addDonation(int dID)												// Adds a new donation for the current donor
{
	char c;
	float amount;
	int dept;
	Donor tempDonor = *Donors.at(dID);
	do
	{
		system("cls");
		std::cout << "Enter Donation amount:" << std::endl;
		//while (!(std::cin >> amount) && !(std::isnan(amount)))
		//{
		//	std::cin.clear();
		//	std::cin.ignore(99999999.999, '\n');
		//	std::cout << "Invalid entry. Please enter an amount:" << std::endl;
		//}
		bool legal = false;
		std::string amt;
		while (!legal) {
			try {
				std::cin >> amt;
				amount = std::stof(amt);
				legal = true;
			}
			catch (const std::exception& e) {
				legal = false;
				std::cout << std::endl << "Not a valid amount. Please enter a donation amount:" << std::endl;
			}
		}
		std::cout << "Select Department to apply donation to:\n1: Computer Science\n2: English\n3: Math\n4: Nursing" << std::endl;
		while (!(std::cin >> dept))
		{
			std::cin.clear();
			std::cin.ignore(99999999, '\n');
			std::cout << "Invalid entry.  Please select a dept:\n1: Computer Science\n2: English\n3: Math\n4: Nursing" << std::endl;
		}
		std::cout << "$" << amount << " for " << Dept[--dept] << "\nIs this correct? (y/n)" << std::endl;
		std::cin >> c;
		std::cin.ignore();
	} while (c != 'Y' && c != 'y');
	tempDonor.addTransaction(tID, amount, dept);
	++tID;
}

void addDonors()													// Adds a new donor
{
	char c;
	do
	{
		std::string last;
		std::string first;
		do
		{
			system("cls");
			std::cout << "Enter Donor's Last name:" << std::endl;
			std::getline(std::cin, last);
			std::cout << "Enter Donor's First name:" << std::endl;
			std::getline(std::cin, first);
			std::cout << "Donor name is: " << first << " " << last << ".\n Is this correct? (y/n)" << std::endl;
			std::cin >> c;
			std::cin.ignore();
		} while (c != 'Y' && c != 'y');
		Donors.insert(std::make_pair(dID, new Donor(first, last)));
		do                           // Do loop to keep asking about another donation
		{
			addDonation(dID);
			std::cout << "Would you like to add another donation? (y/n)" << std::endl;
			std::cin >> c;
			std::cin.ignore();
		} while (c != 'N' && c != 'n');
		system("cls");
		std::cout << first << " " << last << " made the following donations: (ID, Dept, Amount)\n" << std::endl;
		displayDonations(dID);
		++dID;
		std::cout << "Would you like to add another Donor? (y/n)" << std::endl;
		std::cin >> c;
		std::cin.ignore();
	}while (c != 'N' && c != 'n');
}

void displayDonations(int dID)
{
	Donor tempDonor = *Donors.at(dID);
	std::map<int, Transaction*> &tempMap = tempDonor.getMap();
	std::cout << tempDonor.getName() << std::endl;
	for (std::map<int, Transaction*>::iterator it = tempMap.begin(); it != tempMap.end(); ++it)
	{
		Transaction tempTran = *it->second;
		std::cout << "\tID: " << it->first << " " << Dept[tempTran.get_dept()] << ": $" << tempTran.get_amount() << std::endl;
	}
	system("pause");
	return;
}

void displayDonor(int dID)																// Displays selected Donor
{
	Donor tempDonor = *Donors.at(dID);
	std::cout << tempDonor.getName() << std::endl;
	displayDonations(dID);
}

void displayDonorList()																// Displays a list of all Donors
{
	int ID = 0;
	for (std::map<int, Donor*>::iterator it = Donors.begin(); it != Donors.end(); ++it)
	{
		Donor tempDonor = *it->second;
		std::cout << ID++ << " " << tempDonor.getName() << std::endl;
	}
}

void editDonation(int eID, int tID)												// Adds a new donation for the current donor
{
	char c;
	Donor tempDonor = *Donors.at(eID);
	std::map<int, Transaction*> &tempMap = tempDonor.getMap();
	Transaction tempTran = *tempMap.at(tID);
	float amount;
	int dept = tempTran.get_dept();
	do
	{
		system("cls");
		std::cout << "Enter new Donation amount:" << std::endl;
		bool legal = false;
		std::string amt;
		while (!legal) {
			try {
				std::cin >> amt;
				amount = std::stof(amt);
				legal = true;
			}
			catch (const std::exception& e) {
				legal = false;
				std::cout << std::endl << "Not a valid amount. Please enter a donation amount:" << std::endl;
			}
		}
		std::cout << "This donation is currently for: " << Dept[dept] << "\nWould you like to change the Department? (y/n)" << std::endl;
		std::cin >> c;
		std::cin.ignore();
		if (c == 'Y' || c == 'y')
		{
			std::cout << "Select Department to apply donation to:\n1: Computer Science\n2: English\n3: Math\n4: Nursing" << std::endl;
			while (!(std::cin >> dept))
			{
				std::cin.clear();
				std::cin.ignore(99999999, '\n');
				std::cout << "Invalid entry.  Please select a dept:\n1: Computer Science\n2: English\n3: Math\n4: Nursing" << std::endl;
			}
			--dept;
		}
		std::cout << "$" << amount << " for " << Dept[dept] << "\nIs this correct? (y/n)" << std::endl;
		std::cin >> c;
		std::cin.ignore();
	} while (c != 'N' && c != 'n');
	tempTran.change_amount(amount);
	tempTran.change_dept(dept);
}

void deleteDonation(int eID, int tID)												// Adds a new donation for the current donor
{
	char c;
	Donor tempDonor = *Donors.at(eID);
	std::map<int, Transaction*> &tempMap = tempDonor.getMap();
	Transaction tempTran = *tempMap.at(tID);
	system("cls");
	std::cout << "Delete the following donation? (y/n)\n" << "$" << tempTran.get_amount() << " for " << Dept[tempTran.get_dept()] << std::endl;
	std::cin >> c;
	std::cin.ignore();
	if (c == 'Y' || c == 'y')
	{
		tempDonor.deleteTransaction(tID);
	}
}
