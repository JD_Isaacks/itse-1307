/*
++++Game Scores Calculator++++
Chapter 4 & 5 Assignment

This calculator will calculate the winner of three games.

Written by James Isaacks for San Jacinto College South, ITSE 1307
Last good compile on 4/5/17 9:24am
*/

#include <algorithm>
#include <array>
#include <cstdio>
#include <fstream>
#include <iostream>
#include <numeric>
#include <string>
#include <vector>

// Define a limited number of gloabls
std::string games[3];
std::vector <std::string> players;
std::vector < std::vector <int> > scores;
bool gameRun = 0;

// Define the functions we will be using.
void calcMenu();
void gameWarn();
void getData();
void getGames();
void highScore(int j);
void loadGame();
void mainMenu();
void newGame();
void playerAvg();
void saveGame();
void showGames();
void showPlayers();

// Main will only be used to call other functions.
int main()
{
	mainMenu();
	return 0;
}

void calcMenu()
{
	int i;
	std::cout << "This calculator can do several things. What would you like to do?\n\n"
		"1: Display all scores.\n"
		"2: Show by game.\n"
		"3: Show by player.\n"
		"4: Main menu\n";
	std::cin >> i;
	system("cls");
	bool exit = false;
		switch (i)
		{
		case 1:
			playerAvg();
			break;
		case 2:
			showGames();
			break;
		case 3:
			showPlayers();
			break;
		case 4:
			exit = true;
			mainMenu();
			break;
		default:
			calcMenu();
			system("pause");
		}
}

void gameWarn()														// Warn the user that a calculator is in progress
{
	if (gameRun == 1)
	{
		char save;
		std::cout << "You already have a calculator running!\nIf you have not saved this calculator you will lose all data.\nWould you like to save now? [y/n]\n" << std::endl;
		std::cin >> save;
		if (save == 'y' || save == 'Y')
		{
			saveGame();
		}
		system("cls");
	}
}

void getData()														// Gets the data from the users
{
	char done;
	int i = 1;
	int j = 0;
	do
	{
		done = ' ';
		std::string pName;
		std::vector <int> pScore;
		system("cls");
		std::cin.ignore();
		std::cout << "Please enter the name of Player " << i << ":" << std::endl;
		std::getline(std::cin, pName);
		players.push_back(pName);

		for (int k = 0; k < 3; k++)
		{
			int score;
			std::cout << "Please enter score for " << games[k] << ":" << std::endl;
			while (!(std::cin >> score))
			{
				std::cin.clear();
				std::cin.ignore(99999999, '\n');
				std::cout << "Invalid entry. Please enter score for " << games[k] << ":" << std::endl;
			}

			pScore.push_back(score);
		}

		scores.push_back(pScore);
		system("cls");
		std::cout << "Would you like to add another player? [y/n]\n";
		std::cin >> done;
		++i;
		++j;
	} while (done != 'n');
}

void getGames()														// Gets names of three games.
{
	char done;
	do
	{
		done = 'n';
		system("cls");
		std::cin.ignore();
		std::cout << "Please enter the names of the three games to calculate.\n\nFirst Game:\n";
		std::getline(std::cin, games[0]);
		std::cout << "Second Game:\n";
		std::getline(std::cin, games[1]);
		std::cout << "Third Game:\n";
		std::getline(std::cin, games[2]);
		system("cls");
		std::cout << "Game 1\tGame 2\tGame 3" << std::endl;
		for (int i = 0; i < 3; ++i)
		{
			std::cout << games[i] << "\t";
		}
		do
		{
			//std::cin.ignore();
			std::cout << "\nIs this correct? (y/n)";
			std::cin >> done;
		} while (done != 'y' && done != 'n');
	} while (done != 'y');
	system("cls");
}

void highScore(int j)												// Determines and displays the highest score per game.
{
	std::vector<int> high;
	int score;
	for (signed int i = 0; i < scores.size(); ++i)
	{
		score = scores[i][j];
		high.push_back(score);
	}
	std::vector<int>::iterator max;
	max = std::max_element(high.begin(), high.end());
	__int64 k = std::distance(high.begin(), max);
	std::cout << "Player " << players[k] << " won " << games[j] << " with a high score of: " << high[k] << "!!\n\n";
}
void mainMenu()														// Our main menu. Can be brought up in the middle of a session.
{
	int i;
	system("cls");
	std::cout << "++++++ Game Scores Calculator ++++++\n\n"

		"This program calculates the winner of 3-game challenges:\n"
		"1: New\n"
		"2: Load\n"
		"3: Save\n";
	if (gameRun == 1)
	{
		std::cout << "4: Return\n";
	}
	else
	{
		std::cout << "4: Exit\n\n";
	}
	std::cin >> i;
	if (gameRun == 1)
	{
		++i;
	}
	system("cls");
	switch (i)
	{
	case 1:
		newGame();
	case 2:
		loadGame();
	case 3:
		saveGame();
	case 4:
		calcMenu();
	case 5:
		return;
	default:
		mainMenu();
	}
}

void playerAvg()													// Takes an average of each player's data and displays it.
{
	std::cout << "\t\t\t" << games[0] << "\t" << games[1] << "\t" << games[2] << "\tAverage" << std::endl;
	for (int i = 0; i < players.size(); ++i)
	{
		std::cout << players[i] << "\n" "\t";
		for (int j = 0; j < 3; ++j)
		{
			std::cout << scores[i][j] << "\t";
		}

		std::cout << std::accumulate(scores[i].begin(), scores[i].end(), 0.0) / scores[i].size() << "\n\n";
	}
	for (int x = 0; x < 3; ++x)
	{
		highScore(x);
	}
	std::cin.ignore();
	system("pause");
	calcMenu();
}

void showGames()													// Displays a list of all scores for the selected game.
{
	system("cls");
	int j;
	do
	{
		std::cout << "Please select game:\n"
			"1: " << games[0] <<
			"\n2: " << games[1] <<
			"\n3: " << games[2] << std::endl;
		std::cin >> j;
	} while (j < 0 && j > 3);
	std::cout << "The scores for " << games[j] << " are:\n\n";
	for (int i = 0; i < scores.size(); ++i)
	{
		std::cout << players[i] << "\t" << scores[i][j] << "\n";
	}
	std::cout << "\n";
	highScore(j);
	system("pause");
	calcMenu();
}

void showPlayers()													// Shows a list of players in the calculator and lets the user select one to display that player's score.
{
	system("cls");
	std::cout << "Please select a player:\n";
	for (int i = 0; i < players.size(); ++i)
	{
		std::cout << i + 1 << ": " << players[i] << "\n";
	}
	int j;
	do
	{
		std::cout << "Please select a player:\n";
		std::cin >> j;
	} while (j < 1 || j > players.size());
	std::cout << "\t\t" << games[0] << "\t" << games[1] << "\t" << games[2] << std::endl;
	std::cout << players[j] << ": " << scores[j][0] << "\t" << scores[j][1] << "\t" << scores[j][2] << std::endl;
	system("pause");
	calcMenu();
}

// All of our file functions are here:
void newGame()														// Our new game wrapper function.
{
	system("cls");
	gameWarn();
	gameRun = 1;
	getGames();
	getData();
	calcMenu();
	mainMenu();
}

void loadGame()														// Load a calculator file.
{
	gameWarn();
	std::cout << "This feature is not currently working.";
	mainMenu();
}
void saveGame()														// Save the game challenge to a file.
{
	if (gameRun == 1)												// Sanity check to make sure a game is running to be saved.
	{
		saveGame();
	}
	else
	{
		std::cout << "No calculators running!";
		system("pause");
		mainMenu();
	}
	std::cout << "This feature is not currently working.";
	mainMenu();
}