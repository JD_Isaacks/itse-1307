// +++++++++++++++++++++++++++++++++++++++++++++++++++
// ++					POWERBALL					++
// ++ A small program to play a Powerball without	++
// ++ cash. 18+ only, Play Responsibly.				++
// ++												++
// ++ Created by James Isaacks, San Jac South		++
// ++ Last good compile: 9:03pm 2/13/2017			++
// +++++++++++++++++++++++++++++++++++++++++++++++++++

#include <iostream>
#include <cstdlib>
#include <ctime>;

//Setup global values
int i = 0;
int x = 0;
int uValue = 0;
int uPowerNum = 0;
int gPowerNum = 0;
int winCount = 0;
int winPbCount = 0;
int uBallNum[5] = { 0, 0, 0, 0, 0 };
int gBallNum[5] = { 0, 0, 0, 0, 0 };
bool check = false;

//Don't ask. Use in case of loop failure
void herpderp()
{
	for (int x = 0; x < 1000000; x++) {}
}

// Check to see if user input is valid
void isUinValid(int num)
{
	check = true;									// set check==false everytime we call isValid
	if (num > 0 && num < 70)						// first check to see if the user input a number in the valid range
	{
		x = 0;
		while (x <= 4)
		{
			if (num != uBallNum[x])					// now compare input value to every ball
			{
				x++;
			}
			else									// Number already used, ask user for new input by breaking back to uInput()
			{
				check = false;
				std::cout << "You may only use a number once, try again." << std::endl;
				break;
			}
		}
		if (check == true)							// If the number is good, store it
		{
			uBallNum[i] = num;
			i++;
		}
	}
	else											// Bad number, ask user for new input
	{
		check = false;
		std::cout << "Invalid number, try again." << std::endl;
	}
}

// Create random numbers and store
void generate()
{
	x = 0;
	int gValue = 0;
	srand(static_cast<unsigned int>(time(0)));		// random number seed
	while (x <= 4)									// Make 5 random numbers and store
	{
		gValue = rand() % 69 + 1;					// Random number between 1 & 69
		if (gValue != gBallNum[0] || gValue != gBallNum[1] || gValue != gBallNum[2] || gValue != gBallNum[3] || gValue != gBallNum[4])
		{
			gBallNum[x] = gValue;
			x++;
		}
	}
	gPowerNum = rand() % 26 + 1;						// Random number between 1 & 26
}

// Lastly, output the results
void results(int num)
{
	system("cls");
	std::cout << "Todays winning numbers are:" << std::endl;
	std::cout << " *****************************    **\n"
				 "* " << gBallNum[0] << " ** " << gBallNum[1] << " ** " << gBallNum[2] << " ** " << gBallNum[3] << " ** " << gBallNum[4] << " *      ** " << gPowerNum << " **\n"
				 " *****************************    **\n\n";
	std::cout << "Your picks were: \n";
	std::cout << " *****************************    **\n"
				 "* " << uBallNum[0] << " ** " << uBallNum[1] << " ** " << uBallNum[2] << " ** " << uBallNum[3] << " ** " << uBallNum[4] << " *      ** " << uPowerNum << " **\n"
			     " *****************************    **\n";

	switch (num)
	{
	case 1:	std::cout << "You won $4 !!" << std::endl;
		break;
	case 2:	std::cout << "You won $7 !!" << std::endl;
		break;
	case 3:	std::cout << "You won $100 !!" << std::endl;
		break;
	case 4:	std::cout << "You won $50,000 !!" << std::endl;
		break;
	case 5:	std::cout << "You won $1,000,000 !!" << std::endl;
		break;
	case 6:  std::cout << "YOU ARE THE WINNER!!!" << std::endl;
		break;
	case 7:	std::cout << "Sorry, You did not win." << std::endl;
		break;
	}
}

// Lets compare our balls!
void compareBalls()
{
	// Determine if any user numbers match generated numbers
	x = 0;
	winCount = 0;
	winPbCount = 0;
	while (x <= 4)
	{
		if (uBallNum[x] == gBallNum[x])
		{
			winCount++;
		}
		else {}
		x++;
	}
	if (uPowerNum == gPowerNum)
	{
		winPbCount++;
	}
	else {}
}
	// Now compare to determine winner payout
void isWinner()
{
	if (winPbCount == 1)							// 
	{
		if (winCount == 5)
		{
			results(6);
		}
		else if (winCount == 4)
		{
			results(4);
		}
		else if (winCount == 3)
		{
			results(3);
		}
		else if (winCount == 2)
		{
			results(2);
		}
		else if (winCount <= 1)
		{
			results(1);
		}
	}
	else
	{
		if (winCount == 5)
		{
			results(5);
		}
		else if (winCount == 4)
		{
			results(3);
		}
		else if (winCount == 3)
		{
			results(2);
		}
		else
		{
			results(7);
		}
	}
}

// Ask the user for their picks
void uInput()
{
	i = 0;									// Clear out our variable for the while loop
	while (i <= 4)							// Loops 5 times to ask the user for input
	{
		std::cout << "Please enter a number (1-69) for ball " << (i + 1) << ":" << std::endl;
		std::cin >> uValue;
		isUinValid(uValue);
	}
	do										// Use the DO loop to ask for the Powerball number and check it
	{
		std::cout << "Please enter a number (1-26) for the powerball:" << std::endl;
		std::cin >> uValue;
		if (uValue > 0 && uValue < 27) {}	// Check if user input a good number 
		else
		{
			std::cout << "Invalid number, try again.";
		}
	} while (uValue < 0 && uValue > 27);	// End condition makes sure the loop does not end with a bad number
	uPowerNum = uValue;
}

// Here it all comes together
int main()
{
	// Title piece
	std::cout << " _____   ______          ________ _____  ____          _      _      \n"
		"|  __ \\ / __  \\ \\        / /  ____|  __ \|  _ \\   /\\   | |    | |     \n"
		"| |__) | |  | |\\ \\  //\\  / /| |__  | |__) | |_) | /  \\  | |    | |     \n"
		"|  ___/| |  | | \\ \\/  \\/ / |  __| |  _  /|  _ < / /\\ \\ | |    | |     \n"
		"| |    | |__| | \\  /\\  /  | |____| | \ \| |_) / ____ \\| |____| |____ \n"
		"|_|     \\____/   \\/  \\/   |______|_|  \_\____/_/    \\_\\______|______|\n";
	system("pause");
	std::cout << " __                    __   ___  __   __   __        __     __           \n"
		"|__) |     /\\  \\ /    |__) |__  /__` |__) /  \ |\\ | /__` | |__) |    \\ / \n"
		"|    |___ /~~\\  |     |  \ |___ .__/ |    \__/ | \\| .__/ | |__) |___  |  \n";
	system("pause");
	system("cls");

	// Call our functions:
	generate();
	uInput();
	compareBalls();
	isWinner();

	// Pause so the looser can cry over their waste of money and the fact that they will never ammount to anything.
	system("pause");

	// Done
	return 0;
}
