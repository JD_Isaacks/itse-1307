/*  Average Temperature Calculator
        Displays the average temperatures for the week of March 2nd thru the 6th.
    Created by James D Isaacks, San Jacinto College South, ITSE 1307
    Last Compiled January 31, 2017
*/

#include <iostream>

int main ()
{
// Hack in an array of days and temps for March 2-6, 2015 | Last 5-day (business) week that starts on the 2nd found in March recently. 
// Data obtained from https://www.wunderground.com/history/airport/KHOU/2015/3/2/WeeklyHistory.html

    enum avgs {mon=54, tue=68, wed=63, thur=44, fri=44};
   
// Print out the average temperatures per day.
    std::cout << "The average temperatures for the week of Monday March 2, 2015 were:\n" << std::endl;
    std::cout << "Monday: " << mon << std::endl;
    std::cout << "Tuesday: " << tue << std::endl;
    std::cout << "Wednesday: " << wed << std::endl;
    std::cout << "Thursday: " << thur << std::endl;
    std::cout << "Friday: " << fri << std::endl;

// Perform a number of dirty calculations to get averages.
    
    int wkAvg = (mon+tue+wed+thur+fri)/5;
    
// Print out the week average.

    std::cout << "\n The average temperature for the week was " << wkAvg << std::endl;
    
// Hack in a pause.
    system("pause");
    return 0;
}
